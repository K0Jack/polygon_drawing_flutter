import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class Polygon extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PolygonState();
  }
}

class PolygonState extends State<Polygon> {
  var _sides = 1.0;
  var _radius = 100.0;
  var _radians = 0.0;
  Color colors = Colors.blue;
  double number = 1;

  @override
  Widget build(BuildContext context) {
    List<Color> listColor = [Colors.black, Colors.red, Colors.green, Colors.blue];
    List<Text> text = [Text('Black'), Text('Red'), Text('Green'), Text('Blue')];
    return Scaffold(
      appBar: AppBar(
        title: Text('Polygons Drawing'),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: CustomPaint(
                painter: ShapePainter(_sides, _radius, _radians, colors),
                child: Container(),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Text('Enter your number '),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 40,
              child: TextFormField(
                controller: TextEditingController(text: number.toString()),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  double data = double.parse(value);
                  setState(() {
                    _sides = data;
                  });
                },
              ),
            ),
            SizedBox(
              height: 20
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(onTap: (){
                  setState(() {
                    number++;
                    _sides = number;

                  });
                },
                  child: Container(
                    height: 40,width: 40,
                    decoration: BoxDecoration(
                      color: Colors.blue
                    ),
                    child: Center(
                      child: Text(
                        "+",
                        style: TextStyle(
                          fontSize: 20,
                            color: Colors.white
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 80),
                InkWell(
                  onTap: (){
                    if(number <= 2){
                      number = 2;
                    }
                    setState(() {
                      number--;
                      _sides = number;
                    });
                  },
                  child: Container(
                    height: 40,width: 40,
                    decoration: BoxDecoration(
                        color: Colors.blue
                    ),
                    child: Center(
                      child: Text(
                          "-",
                        style: TextStyle(
                            fontSize: 20,
                          color: Colors.white
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 50)),
          ],
        ),
      ),
    );
  }
}

class ShapePainter extends CustomPainter {
  final double sides;
  final double radius;
  final double radians;
  final Color colors;

  ShapePainter(this.sides, this.radius, this.radians, this.colors);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = colors
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    var path = Path();

    var angle = (math.pi * 2) / sides;

    Offset center = Offset(size.width / 2, size.height / 2);
    Offset startPoint = Offset(radius * math.cos(radians), radius * math.sin(radians));

    path.moveTo(startPoint.dx + center.dx, startPoint.dy + center.dy);

    for (int i = 1; i <= sides; i++) {
      double x = radius * math.cos(radians + angle * i) + center.dx;
      double y = radius * math.sin(radians + angle * i) + center.dy;
      path.lineTo(x, y);
    }
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
